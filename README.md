<p>
    <a href="https://count.getloli.com/"><img src="https://count.getloli.com/get/@:lovobin">
        <imgsrc="https://weather-icon.journeyad.repl.co/@xianyang?v=1" align="right"></a>
    </p>
<h1 align="center">Hi 👋, I'm Chen</h1><h3 align="center">Learning is the eternal motivation！</h3>
      <p>
        All at sea.<br />
        希望能成为一个有趣的人。希望能创造出可以成为他人回忆的事物。<br />
        <i style="text-decoration: line-through">25岁，是打工人</i> =>
        成长ing....<br />
        在下年满十八，遵纪守法，爱国爱家，不碰烟(就碰酒)，善养鸡鸭，什么都爱吃的吃货。<br />
        爱好：ACGN、科幻、诗歌、开源、Web（以及一切有趣的东西）、摸鱼<br />
        喜欢：星空、看电影、膜拜成龙、摄影、西瓜、旅行<br />
        讨厌：无审美之人(流量明星,giegie) <br />
        运动：徒步旅行，乒乓球🏓<br />
        (因为本死宅也不会啥别的运动了吧……)<br />
        目标：前端工程师 || 全栈工程师 || AI沙雕开发者 || 运维工程师 <br />
        梦想：摄影师 && 诗歌作家 && 独立开发制作人 && 全职开源作者 &&
        🦸‍♂️（皆为不切实际的）<br />
      </p>
      
<h1 align="center">a 𝑭𝒓𝒐𝒏𝒕-𝒆𝒏𝒅 𝑬𝒏𝒈𝒊𝒏𝒆𝒆𝒓  </h1>

座右铭：学如逆水行舟，不进则退！

## 𝗠𝘆 𝗧𝗲𝗰𝗸 𝗦𝘁𝗮𝗰𝗸

[![OS](https://img.shields.io/badge/OS-macOS-informational?style=flat-square&logo=apple&logoColor=white)](https://en.wikipedia.org/wiki/MacOS)
[![OS](https://img.shields.io/badge/OS-Linux-informational?style=flat-square&logo=linux&logoColor=white)](https://en.wikipedia.org/wiki/Linux)
[![Editor](https://img.shields.io/badge/Editor-VSCode-blue?style=flat-square&logo=visual-studio-code&logoColor=white)](https://code.visualstudio.com/)
[![](https://img.shields.io/website?color=0ab9e6&style=flat-square&up_message=Chen's-Blog&url=https://yangchaoyi.vip/)](https://seachen.cn/)

![HTML5](https://img.shields.io/badge/-HTML5-%23E44D27?style=flat-square&logo=html5&logoColor=ffffff)
![CSS3](https://img.shields.io/badge/-CSS3-%231572B6?style=flat-square&logo=css3)
![JavaScript](https://img.shields.io/badge/-JavaScript-%23F7DF1C?style=flat-square&logo=javascript&logoColor=000000&labelColor=%23F7DF1C&color=%23FFCE5A)
![Vue.js](https://img.shields.io/badge/-Vue.js-%232c3e50?style=flat-square&logo=Vue.js)
![React](https://img.shields.io/badge/-React-%23282C34?style=flat-square&logo=react)
![Webpack](https://img.shields.io/badge/-Webpack-%232C3A42?style=flat-square&logo=webpack)
![ESlint](https://img.shields.io/badge/-ESLint-%234B32C3?style=flat-square&logo=eslint)


![Sass](https://img.shields.io/badge/-Sass-%23CC6699?style=flat-square&logo=sass&logoColor=ffffff)
![Stylus](https://img.shields.io/badge/-Stylus-%23333333?style=flat-square&logo=stylus)
<img alt="React" src="https://img.shields.io/badge/-React-45b8d8?style=flat-square&logo=react&logoColor=white" />
<img alt="Docker" src="https://img.shields.io/badge/-Docker-46a2f1?style=flat-square&logo=docker&logoColor=white" />
<img alt="github actions" src="https://img.shields.io/badge/-Github_Actions-2088FF?style=flat-square&logo=github-actions&logoColor=white" />
<img alt="TypeScript" src="https://img.shields.io/badge/-TypeScript-007ACC?style=flat-square&logo=typescript&logoColor=white" />
<img alt="redux" src="https://img.shields.io/badge/-Redux-764ABC?style=flat-square&logo=redux&logoColor=white" />

![Nodejs](https://img.shields.io/badge/-Nodejs-43853d?style=flat-square&logo=Node.js&logoColor=white)
<img alt="GraphQL" src="https://img.shields.io/badge/-GraphQL-E10098?style=flat-square&logo=graphql&logoColor=white" />
<img alt="Sass" src="https://img.shields.io/badge/-Sass-CC6699?style=flat-square&logo=sass&logoColor=white" />
<img alt="Styled Components" src="https://img.shields.io/badge/-Styled_Components-db7092?style=flat-square&logo=styled-components&logoColor=white" />

![Git](https://img.shields.io/badge/-Git-%23F05032?style=flat-square&logo=git&logoColor=%23ffffff)
<img alt="NestJs" src="https://img.shields.io/badge/-NestJs-ea2845?style=flat-square&logo=nestjs&logoColor=white" />
<img alt="angular" src="https://img.shields.io/badge/-Angular-DD0031?style=flat-square&logo=angular&logoColor=white" />
<img alt="npm" src="https://img.shields.io/badge/-NPM-CB3837?style=flat-square&logo=npm&logoColor=white" />
<img alt="Rollup" src="https://img.shields.io/badge/-Rollup-EC4A3F?style=flat-square&logo=rollup.js&logoColor=white" />
<img alt="Prettier" src="https://img.shields.io/badge/-Prettier-F7B93E?style=flat-square&logo=prettier&logoColor=white" />
<img alt="MongoDB" src="https://img.shields.io/badge/-MongoDB-13aa52?style=flat-square&logo=mongodb&logoColor=white" />


## 𝗦𝘁𝗮𝘁𝘀

<p align="left">
<img alt="ouuan's github stats" height='230' src="https://github-readme-stats.vercel.app/api?username=DreamStar1996&show_icons=true&include_all_commits=true">
<img alt="ouuan's github stats" height='230' src="https://github-readme-stats.vercel.app/api/top-langs/?username=DreamStar1996">
</p>
